package com.netkracker.sproot.service;

import com.netkracker.sproot.entity.User;

public interface UserService {

    User getUser(String login);

}
