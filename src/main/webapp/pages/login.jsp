<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Spring Security</title>

    <link href="<c:url value="/pages/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/pages/css/signin.css" />" rel="stylesheet">

</head>

<body>

<div class="container">
    <div class="jumbotron" style="margin-top: 20px;">
        <c:url value="/j_spring_security_check" var="loginUrl" />
        <form action="${loginUrl}" method="post">
            <h2 class="form-signin-heading">Please sign in</h2>
            <input type="text" class="form-control" name="j_username" placeholder="Email address" >
            <input type="password" class="form-control" name="j_password" placeholder="Password" >
            <button type="submit" class="btn btn-lg btn-primary btn-block" >Войти</button>
        </form>
    </div>

    <div class="footer">
        <p>Sproot 2017</p>
    </div>
</div>


</body>
</html>